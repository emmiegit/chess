## mallard-chess

<p align="center">
  <a href="https://github.com/ammongit/chess/actions?query=workflow%3A%22Build%22">
    <img src="https://github.com/ammongit/chess/workflows/Build/badge.svg"
         alt="Build status">
  </a>
</p>

Chess engine wrapper utility created for fun. Requires [stockfish](https://stockfishchess.org/) to be installed and available in your `$PATH`.

```
cargo run --release -- [options]
```

Run with `--help` for command-line usage.
